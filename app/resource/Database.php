<?php 
$username = 'person';
$dsn = 'mysql:dbname=people;host=mysql';
$password = 'secret';

try{

  $db = new PDO($dsn, $username, $password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

}catch(PDOException $exception) {
  echo "[EXCEPTION] Connection Failed: ".$exception->getMessage();
}

