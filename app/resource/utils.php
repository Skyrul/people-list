<?php 



function check_empty_fields($required_fields_array){
  $form_errors = array();
  foreach($required_fields_array as $name_of_field) {
    if(!isset($_POST[$name_of_field]) || ($_POST[$name_of_field] == NULL)) {
      $form_errors[] = $name_of_field." is a required field.";
    }
  }
  return $form_errors;
}

function check_minimum_length($fields_to_check) {
  $form_errors = array();
  foreach($fields_to_check as $name_of_field => $minimum_length_required) {
    if(strlen(trim($_POST[$name_of_field])) < $minimum_length_required) {
      $form_errors[] = $name_of_field." is too short, must be {$minimum_length_required} characters long. ";
    }
  }
  return $form_errors;
}

function check_email($data) {
  $form_errors = array();
  $key = 'email';

  if(array_key_exists($key, $data)) {

    if($_POST[$key] != null) {

      $key = filter_var($key, FILTER_SANITIZE_EMAIL);

      if(filter_var($_POST[$key], FILTER_VALIDATE_EMAIL) === false) {
        $form_errors[] = $key." is not a valid email address.";
      }
    }
  }

  return $form_errors;
}

function show_errors($form_errors_array) {
  $errors = "<div class='alert alert-danger'><ul style='color:red;'>";

  foreach($form_errors_array as $the_error) {
    $errors .= "<li> {$the_error} </li>";
  }

  $errors .= "</ul></div>";
  return $errors;
}

function flashMessage($message, $passOrFail="Fail") {
  if($passOrFail === "Pass") {
    $data = "<div class='alert alert-success'>{$message}</div>";
  }
  else{
    $data = "<div class='alert alert-danger'>{$message}</div>";
  }
  return $data;
}

function redirectTo($page) {
  header("Location: {$page}.php");
}

function checkDuplicateEntries($table, $column_name, $value, $db) {
  try{
    $sqlQuery = "SELECT * FROM ".$table." WHERE ".$column_name."=:column_name";
    $statement = $db->prepare($sqlQuery);
    $statement->execute(array(':column_name' => $value));
    if($row = $statement->fetch()) {
      return true;
    }
    return false;
  }catch (PDOException $exception){ }
}

function rememberMe($user_id) {
  $encryptCookieData = base64_encode("Hjm8NHfhOjkktN3YJOWrXq${user_id}");
  setCookie("rememberUserCookie", $encryptCookieData, time()+60*60*24*100, "/");
}

function isCookieValid($db) {
  $isValid = false;
  if(isset($_COOKIE['rememberUserCookie'])) {
    $decryptCookieData = base64_decode($_COOKIE['rememberUserCookie']);
    $user_id = explode("Hjm8NHfhOjkktN3YJOWrXq", $decryptCookieData);
    $userID = $user_id[1];

    $sqlQuery = "SELECT * FROM users WHERE id = :id";
    $statement = $db->prepare($sqlQuery);
    $statement->execute(array(':id' => $userID));

    if($row = $statement->fetch()) {
      $id = $row['id'];
      $username = $row['username'];

      $_SESSION['id'] = $id;
      $_SESSION['username'] = $username;
      $isValid = true;
    }
    else {
      $isValid = false;
      signout();
    }
  }
  return $isValid;
}

function signout() {
  unset($_SESSION['username']);
  unset($_SESSION['id']);

  if(isset($_COOKIE['rememberUserCookie'])) {
    unset($_COOKIE['rememberUserCookie']);
    setcookie('rememberUserCookie', null, -1, '/');
  }

  session_destroy();
  // session_regenerate_id(true);
  redirectTo('index');
}

function guard() {
  $isValid = true;
  $inactive = 60 * 59;
  $fingerprint = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);

  if(isset($_SESSION['fingerprint']) && ($_SESSION['fingerprint'] != $fingerprint)) {
    $isValid = false;
    signout();
  }
  else if(isset($_SESSION['last_active']) && ((time() - $_SESSION['last_active']) > $inactive) && $_SESSION['username']){
    $isValid = false;
    signout();
  }
  else {
    $_SESSION['last_active'] = time();
  }
  return $isValid;
}

function isValidImage($file) {
  $form_errors = array();

  $part = explode(".", $file);
  $extension = end($part);

  switch(strtolower($extension)) {
    case 'jpg':
    case 'gif':  
    case 'bmp':
    case 'png':
    
    return $form_errors;
  }

  $form_errors[] = $extension." is not a valid image extension.";
  return $form_errors;
}

function uploadPicture($username) {  
  if($_FILES['avatar']['tmp_name']) {
    $temp_file = $_FILES['avatar']['tmp_name'];
    $ext = pathinfo($_FILES['avatar']['name'], PATHINFO_EXTENSION);
    $filename = $username.md5(microtime()).".{$ext}";
    // $path = "..".$ds."public".$ds."uploads".$ds.$avatar_name;
    $path = "uploads/{$filename}";
    move_uploaded_file($temp_file, $path);
    return $path;
  }
  return false;
}

function _token() {
  $randomToken = base64_encode(openssl_random_pseudo_bytes(32));
  return $_SESSION['token']=$randomToken;
}

function validate_token($requestToken) {
  if(isset($_SESSION['token']) && ($requestToken === $_SESSION['token'])) {
    unset($_SESSION['token']);    
    return true;
  }
  
  return false;
}

function getTotalPeople() {
  
$username = 'person';
$dsn = 'mysql:dbname=people;host=mysql';
$password = 'secret';
$db = null;
try{

  $db = new PDO($dsn, $username, $password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

}catch(PDOException $exception) {
  echo "[EXCEPTION] Connection Failed: ".$exception->getMessage();
}



      $result = -404;
      try{

        $sqlCount = "SELECT COUNT(*) AS counter FROM users";

        $statement = $db->prepare($sqlCount);
        $statement->execute(array());

        while($rs = $statement->fetch()) {          
          $result = $rs['counter'];
        }

      }catch(PDOException $exception) {
        $result = flashMessage("An error occurred: ".$exception->getMessage());
      }
      return $result;
  

}
?>