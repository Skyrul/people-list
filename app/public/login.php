<?php 
include_once '../resource/session.php';
include_once './partials/parseLogin.php';

?>

<?php 
$page_title = "People Book System - Log-in";
include_once './partials/headers.php';
?>

<div class="container">
  <section class="col col-lg-7">

    <h2>Log-in Form</h2>
    <hr />
<div>
 <?php  if(isset($result)) { echo $result; } ?>
 <?php  if(!empty($form_errors)) { echo show_errors($form_errors); } ?>
</div>
<div class="clearfix"></div>
<form action="" method="post">
  <div class="form-group">
    <label for="usernameField">Username</label>
    <input type="text" class="form-control" id="usernameField" name="username" placeholder="Username">
  </div>
  <div class="form-group">
    <label for="passwordField">Password</label>
    <input type="password" class="form-control" id="passwordField" name="password" placeholder="Password">
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox" name="remember" value="yes"> Remember Me
    </label>
  </div>
  <input type="hidden" name="token" value="<?php if(function_exists('_token')) {echo _token();} ?>" />
  <a href="forgot_password.php">Forgot Password</a>
  <button type="submit" class="btn btn-primary pull-right" name="log_in_button" >Sign in</button>
</form>

  </section>

</div>
    <?php include_once './partials/footers.php'; ?>
  </body>
</html>