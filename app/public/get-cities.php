<?php 
include_once '../resource/Database.php';
$cities = null;
$result = "failed";
try{
  $sqlQuery = "SELECT city FROM cities_only ORDER BY city ASC";
  $statement = $db->query($sqlQuery);
  $cities = $statement->fetchAll(PDO::FETCH_ASSOC);  
  $result = "success";
} catch(PDOException $exception) {
  
}

$array = array();
$array['result'] = $result;
$array['cities'] = $cities;

$sendmessage = json_encode($array, JSON_PRETTY_PRINT);

echo $sendmessage;

?>