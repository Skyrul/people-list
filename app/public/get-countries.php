<?php 
include_once '../resource/Database.php';
$countries = null;
$result = "failed";
try{
  $sqlQuery = "SELECT country FROM countries ORDER BY country ASC";
  $statement = $db->query($sqlQuery);
  $countries = $statement->fetchAll(PDO::FETCH_ASSOC);  
  $result = "success";
} catch(PDOException $exception) {
  
}

$array = array();
$array['result'] = $result;
$array['countries'] = $countries;

$sendmessage = json_encode($array, JSON_PRETTY_PRINT);

echo $sendmessage;

?>