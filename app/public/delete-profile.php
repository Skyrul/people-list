<?php 
include_once '../resource/Database.php';

$data_received = json_decode($_POST['jsondata']);
$deleteid = $data_received->deleteid;

$sqlQuery = "DELETE FROM users WHERE id = :id";
$statement = $db->prepare($sqlQuery);
$statement->execute(array(':id' => $deleteid));

$result = "failed";
if($statement->rowCount() == 1) {

  $sqlDelete = "DELETE FROM id_ord WHERE id = :id";
  $statement = $db->prepare($sqlDelete);
  $statement->execute(array(':id' => $deleteid));

  if($statement->rowCount() == 1) {
    $result = "success";
  }

}

$array = array();
$array['result'] = $result;

$sendmessage = json_encode($array, JSON_PRETTY_PRINT);

echo $sendmessage;

?>