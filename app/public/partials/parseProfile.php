<?php 
include_once '../resource/Database.php';
include_once '../resource/utils.php';

$profile_picture = null;
$id=null;

if((isset($_SESSION['id']) || isset($_GET['user_identity'])) && !isset($_POST['updateProfileButton'])) { 

  if(isset($_GET['user-identity'])) {
    $url_encoded_id = $_GET['user-identity'];
    $decode_id = base64_decode($url_encoded_id);
    $user_id_array = explode("encodeuserid", $decode_id);

    $id = $user_id_array[1];
  }
  else {
    $id = $_SESSION['id'];
  }

  $sqlQuery = "SELECT * FROM users WHERE id = :id";
  $statement = $db->prepare($sqlQuery);
  $statement->execute(array(':id' => $id));

  while($rs = $statement->fetch()) {
    $username = $rs['username'];
    $email = $rs['email'];
    $date_joined = strftime("%b %d, %Y", strtotime($rs["join_date"]));
    $first_name = $rs['first_name'];
    $last_name = $rs['last_name'];
    $date_birth = $rs['date_birth'];
    $country = $rs['country'];
    $state = $rs['state'];
    $city = $rs['city'];
    $gender = $rs['gender'];
    $hobbies = explode(",", $rs['hobby']);
    $profile_picture = $rs['avatar'];
  }

  $encode_id = base64_encode("encodeuserid${id}");
  
}
else if(isset($_POST['updateProfileButton'], $_POST['token'])) {

  if(validate_token($_POST['token'])) {

    $form_errors = array();
    $required_fields = array('email', 'username');

    $form_errors = array_merge($form_errors, check_empty_fields($required_fields));
    $fields_to_check_length = array('username' => 5);
    $form_errors = array_merge($form_errors, check_minimum_length($fields_to_check_length));
    $form_errors = array_merge($form_errors, check_email($_POST));
    isset($_FILES['avatar']['name']) ? $avatar = $_FILES['avatar']['name'] : $avatar = null;

    if($avatar != null) {
      $form_errors = array_merge($form_errors, isValidImage(($avatar)));
    }

    $email = $_POST['email'];
    $username = $_POST['username'];
    $hidden_id = $_POST['hidden_id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $date_birth = date('Y-m-d', strtotime($_POST['date_birth']));
    $country = $_POST['country'];
    $state = $_POST['state'];
    $city = $_POST['city'];
    $gender = $_POST['gender'];
    $hobbies = $_POST['hobbies']; 
    if((gettype($hobbies) != 'string') && (gettype($hobbies) != 'NULL'))
    { $hobby = implode(",", $hobbies); }

    if(empty($form_errors)) {
      try {

        $avatarQuery = "SELECT avatar FROM users WHERE id = :id";
        $oldAvatarStatement = $db->prepare($avatarQuery);
        $oldAvatarStatement->execute([':id' => $hidden_id]);

        if($rs = $oldAvatarStatement->fetch()) {
          $oldAvatar = $rs['avatar'];
        }

        $sqlUpdate = "UPDATE users SET username = :username, email = :email, first_name = :first_name, last_name = :last_name, date_birth = :date_birth, country = :country, state = :state, city = :city, gender = :gender, hobby = :hobby WHERE id = :id";
        $statement = $db->prepare($sqlUpdate);

        if($avatar != null) {

          $sqlUpdate = "UPDATE users SET username = :username, email = :email, first_name = :first_name, last_name = :last_name, date_birth = :date_birth, country = :country, state = :state, city = :city, gender = :gender, hobby = :hobby, avatar = :avatar WHERE id = :id";

          $picture_path = uploadPicture($username);
          if(!$picture_path) {
            $picture_path = "uploads/default.jpg";
          }

          $statement = $db->prepare($sqlUpdate);
          $statement->execute(array(':username' => $username, ':email' => $email, ':id' => $hidden_id, ':first_name' => $first_name, ':last_name' => $last_name, ':date_birth' => $date_birth, ':country' => $country, ':state' => $state, ':city' => $city, ':gender' => $gender, ':hobby' => $hobby, ':avatar' => $picture_path));

          if(isset($oldAvatar)) {
            unlink($oldAvatar);
          }
                    
        }
        else{

          $statement->execute(array(':username' => $username, ':email' => $email, ':id' => $hidden_id, ':first_name' => $first_name, ':last_name' => $last_name, ':date_birth' => $date_birth, ':country' => $country, ':state' => $state, ':city' => $city, ':gender' => $gender, ':hobby' => $hobby));
          
        }

        if($statement->rowCount() == 1) {
          // $result = "<script type='text/javascript'>swal('Updated!', 'Profile updated successfully', 'success');setTimeout(function(){window.location.href = 'profile.php';}, 3000)</script>";
          $result = "<script type='text/javascript'>swal('Updated!', 'Profile updated successfully', 'success');</script>";
        }
        else{
          $result = "<script type='text/javascript'>swal('Nothing happened', 'Have not made any changes');</script>";
        }
      } catch (PDOException $exception) {
        $result = flashMessage("An error occurred in: ".$exception->getMessage());
      }
      $hobbies = explode(",", $hobby);
    }
    else{
      if(count($form_errors) == 1) {
        $result = flashMessage("There is 1 error in the form<br />");
      }
      else {
        $result = flashMessage("There are ".count($form_errors)." errors in the form<br />");
      }
    }
    
  }else {
    $result = "<script type='text/javascript'>swal('Error', 'This request originates from an unknown source; - possible CSRF attack!', 'error');</script>";
  }


}

?>