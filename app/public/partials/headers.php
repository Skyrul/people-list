<?php 
include_once '../resource/session.php'; 
include_once '../resource/Database.php'; 
include_once '../resource/utils.php'; 
?>
<!DOCTYPE html>
<html>
  <head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php if(isset($page_title)) { echo $page_title; } ?></title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>    

    <script src="../js/headers.js"></script>
  </head>
  <body>

<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">People Book</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav"><i class="hide"><?php echo guard(); ?></i>
            <li><a href="index.php">Home</a></li>

      <?php if(isset($_SESSION['username']) || isCookieValid($db)): ?>
            <li><a href="profile.php">My Profile</a></li>
            <li><a href="logout.php">Log-out</a></li>
      <?php else: ?>
            <li><a href="login.php">Log-in</a></li>
            <li><a href="signup.php">Sign-up</a></li>            
      <?php endif ?>            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>




  </body>
</html>