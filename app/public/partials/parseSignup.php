<?php 
$page_title = "People Book System - Register";
include_once './partials/headers.php';
include_once '../resource/Database.php';
include_once '../resource/utils.php';

if(isset($_POST['sign_up_button'], $_POST['token'])) {

  if(validate_token($_POST['token'])) {

    $form_errors = array();

    $required_fields = array('email', 'password');
    $form_errors = array_merge($form_errors, check_empty_fields($required_fields));

    $fields_to_check_length = array('email'=>5, 'password'=>8);
    $form_errors = array_merge($form_errors, check_minimum_length($fields_to_check_length));

    $form_errors = array_merge($form_errors, check_email($_POST));

      $email = $_POST['email'];
      $password = $_POST['password'];

    if(checkDuplicateEntries("users", "email", $email, $db)) {
      $result = flashMessage("Email is already registered.");
    }
    else if(empty($form_errors)) {

      $hashed_password = password_hash($password, PASSWORD_DEFAULT);
      
      try{

        $sqlInsert = "INSERT INTO users (username,email,password,join_date,avatar) VALUES(:username, :email, :password, now(), 'uploads/default.jpg')";

        $statement = $db->prepare($sqlInsert);
        $statement->execute(array(':username' => $email, ':email' => $email, ':password' => $hashed_password));

        if($statement->rowCount() == 1) {

            $sqlQuery = "SELECT id FROM users WHERE email = :email";
            $statement = $db->prepare($sqlQuery);
            $statement->execute(array(':email' => $email));
            // $id = 0;
            if($row = $statement->fetch()) {
              $id = $row['id'];
            }

            $sqlQuery = "SELECT ord FROM last_entry";
            $statement = $db->prepare($sqlQuery);
            $statement->execute();
            // $ord = 0;
            if($row = $statement->fetch()) {
              $ord = $row['ord'];
            }

            $sqlInsert = "INSERT INTO id_ord (ord, id) VALUES(:ord, :id)";
            $statement = $db->prepare($sqlInsert);
            $statement->execute(array(':ord' => $ord+1, ':id' => $id));

            if($statement->rowCount() == 1) {

              $sqlUpdate = "UPDATE last_entry SET ord = :ord WHERE ord = :ord_prev";
              $statement = $db->prepare($sqlUpdate);
              $statement->execute(array(':ord' => $ord+1, ':ord_prev' => $ord));

              $result = flashMessage("Registration Successful.", "Pass");
              echo $result = "<script type='text/javascript'>swal({title: 'Registration Successful!', text: 'Hello new friend. Please wait for it and you\'ll be redirected to login. ', icon: 'success', timer: 4000, button: 'Yess!'});setTimeout(function(){ window.location.href = 'login.php'; }, 3000); </script>";
            }

        }

      }catch(PDOException $exception) {
        $result = flashMessage("An error occurred: ".$exception->getMessage());
      }
      
    }
    else {
      if(count($form_errors) == 1) {      
        $result = flashMessage("There was one error in the form. <br />");
      }
      else {
        $result = flashMessage("There were ".count($form_errors)." errors in the form <br />");
      }
    }

  }else {
    $result = "<script type='text/javascript'>swal('Error', 'This request originates from an unknown source; - possible CSRF attack!', 'error');</script>";
  }

}