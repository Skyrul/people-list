<?php 
$page_title = "People Book System - Forgot Password";
include_once './partials/headers.php';
?>


<?php 
include_once '../resource/Database.php';
include_once '../resource/utils.php';

if(isset($_POST['passwordResetBtn'], $_POST['token'])) {

  if(validate_token($_POST['token'])) {

    $form_errors = array();

    $required_fields = array('email', 'new_password', 'confirm_password');
    $form_errors = array_merge($form_errors, check_empty_fields($required_fields));

    $fields_to_check_length = array('new_password' => 6, 'confirm_password' => 6);
    $form_errors = array_merge($form_errors, check_minimum_length($fields_to_check_length));

    $form_errors = array_merge($form_errors, check_email($_POST));

    if(empty($form_errors)) {
      $email = $_POST['email'];
      $password1 = $_POST['new_password'];
      $password2 = $_POST['confirm_password'];

      if($password1 != $password2) {
        $result = "<p style='padding:20px; border:1px solid gray; color:red;'> New password and confirm password does not match. </p>";
      }
      else {
        try{
          $sqlQuery = "SELECT email FROM users WHERE email = :email";
          $statement = $db->prepare($sqlQuery);
          $statement->execute(array(':email' => $email));

          if($statement->rowCount() == 1) {
            $hashed_password = password_hash($password1, PASSWORD_DEFAULT);
            $sqlUpdate = "UPDATE users SET password =:password WHERE email =:email";
            $statement = $db->prepare($sqlUpdate);
            $statement->execute(array(':password' => $hashed_password, ':email' => $email));
            $result = "<p style='padding:20px; border:1px solid gray; color:green;'>Password Reset Successful. </p>";
          }
          else {
            $result = "<p style='padding:20px; border:1px solid gray; color:red;'>The email address you provided does not exist in the system. </p>";
          }

        }catch(PDOException $exception){
          $result = "<p style='padding:20px; border:1px solid gray; color:red;'>[EXCEPTION] An error occurred: ".$exception->getMessage().". </p>";
        }
      }
    }
    else {
      if(count($form_errors) == 1) {
        $result = "<div class='alert alert-danger'>There is one error in the form.</div>";
      }
      else{
        $result = "<div class='alert alert-danger'>There are ".count($form_errors)." errors in the form. </div>";
      }
    }

  }else {
    $result = "<script type='text/javascript'>swal('Error', 'This request originates from an unknown source; - possible CSRF attack!', 'error');</script>";
  }


}

?>