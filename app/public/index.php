<?php 
$page_title = "People Book - Home View";
include_once './partials/headers.php';
include_once './partials/parseMembers.php';

?>

<div class="container">
  <div class="flag">
    <h1>People Book System - Home </h1>
    
    <?php if(!isset($_SESSION['username'])): ?>
    
      <p class = "lead">
        <p class="lead">A place to register and add people.<br> All you get is a list of people.</p>
        Please <a href="login.php">Log-in</a> <br />
        Not yet a member? <a href="signup.php">Sign up</a>
      </p>
    <?php else: ?>
    <button id="saveOrderButton" name="saveOrderButton" class="btn btn-primary" style="margin:2rem 2rem;">Save order</button>
      <section class="col col-lg-12">
        <?php if(count($members) > 0): ?>
        <div class="row" style="margin-bottom: 10px;">
          <table class="col-lg-4 table table-striped table-bordered sorted_table">
          <thead>
            <tr>
              <th></th>
              <th></th>
              <th>Name</th>
              <th>Email</th>
              <th>Date of Birth</th>
              <th>Gender</th>
              <th>Address</th>              
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($members as $member): ?>
          <?php $id = $member['id']; ?>
            <tr>
              <td><?php echo $id; ?></td>
              <td><img src="<?php echo $member['avatar']; ?>" style="width:44px; height:44px;" /></td>
              <td><?php echo $member['first_name']." ".$member['last_name']; ?></td>
              <td style="text-align:center;"><?php echo $member['email']; ?></td>
              <td><?php echo strftime("%b %d, %Y", strtotime($member['date_birth'])); ?></td>
              <td><?php echo $member['gender']; ?></td>
              <td><?php echo $member['country'].", ".$member['state'].", ".$member['city']; ?></td>
              <td><a href="edit-profile.php?user-identity=<?php if(isset($id)) { echo base64_encode("encodeuserid".$id); }?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>&nbsp;&nbsp;&nbsp;<a href="javascript: goDelete(<?php echo $id; ?>); " data="testd"><span class="glyphicon glyphicon-trash"></span> Delete</a>&nbsp;</td>
            </tr>
          <?php endforeach; ?>
            <tbody>
          </table>
          </div>
        <?php else: ?>
          <p>No member found!? This is impossible!</p>
        <?php endif; ?>
      </section>
    <?php endif ?>
  </div>
</div>

<?php include_once './partials/footers.php'; ?>
<script src="js/index-footer.js"></script>