<?php 
$page_title = "People Book System - Home";
include_once './partials/headers.php';
?>
    <div class="container">

      <div class="flag">
        <h1>People Book System</h1>
        <p class="lead">A place to register and add people.<br> All you get is a list of people.</p>
      

      <?php if(!isset($_SESSION['username'])): ?>
      <p class="lead">You are currently not signed in <a href="login.php">Log-in</a> Not yet a member? <a href="signup.php">Sign up</a></p>

      <?php else: ?>

      <p class="lead">You are logged in as <?php if(isset($_SESSION['username'])) { echo $_SESSION['username']; } ?> <a href="logout.php">Log-out</a></p>

      <?php endif ?>
        </div>
    </div>

<?php include_once './partials/footers.php'; ?>
  </body>
</html>