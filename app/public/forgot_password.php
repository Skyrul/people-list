<?php 
$page_title = "People Book System - Forgot Password";
include_once './partials/parsePasswordReset.php';

?>
    <p><a href="javascript: history.back();">Back</a></p>


<div class="container">
  <section class="col col-lg-7">

    <h2>People Book System - Reset password</h2>
    <hr />

<div>
 <?php  if(isset($result)) { echo $result; } ?>
 <?php  if(!empty($form_errors)) { echo show_errors($form_errors); } ?>
</div>
<div class="clearfix"></div>

<form action="" method="post">
  <div class="form-group">
    <label for="emailField">Email</label>
    <input type="text" class="form-control" id="emailField" name="email" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="newPasswordField">New Password</label>
    <input type="password" class="form-control" id="newPasswordField" name="new_password" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="confirmPasswordField">Confirm Password</label>
    <input type="password" class="form-control" id="confirmPasswordField" name="confirm_password" placeholder="Confirm Password">
  </div>
  <input type="hidden" name="user_id" value="<?php if(isset($id)){echo $id;} ?>">
  <input type="hidden" name="token" value="<?php if(function_exists('_token')) {echo _token();} ?>" />
  <button type="submit" class="btn btn-primary pull-right" name="passwordResetBtn" >Reset Password</button>
</form>

  </section>

</div>
    <?php include_once './partials/footers.php'; ?>


  </body>
</html>