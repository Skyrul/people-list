<?php 
$page_title = "People Book System - Register";
// include_once './partials/headers.php';
include_once './partials/parseSignup.php';
?>
    <p><a href="javascript: history.back();">Back</a></p>

<div class="container">
  <section class="col col-lg-7">

    <h2>Registration Form</h2>
    <p class="lead">Quick registration<br></p>
    <hr />

<div>
 <?php  if(isset($result)) { echo $result; } ?>
 <?php  if(!empty($form_errors)) { echo show_errors($form_errors); } ?>
</div>
<div class="clearfix"></div>

<form action="" method="post">
  <div class="form-group">
    <label for="emailField">Email</label>
    <input type="text" class="form-control" id="emailField" name="email" placeholder="This email will also be your username">
  </div>
  <!-- <div class="form-group">
    <label for="usernameField">Username</label>
    <input type="text" class="form-control" id="usernameField" name="username" placeholder="Username">
  </div> -->
  <div class="form-group">
    <label for="passwordField">Password</label>
    <input type="password" class="form-control" id="passwordField" name="password" placeholder="Password">
  </div>
  <input type="hidden" name="token" value="<?php if(function_exists('_token')) {echo _token();} ?>" />
  <button type="submit" class="btn btn-primary pull-right" name="sign_up_button" >Sign up</button>
</form>

  </section>

</div>
    <?php include_once './partials/footers.php'; ?>

  </body>
</html>