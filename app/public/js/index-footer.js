const saveOrdered = (() => {
  document.getElementById("saveOrderButton")?.addEventListener('click', function (ev) {
    ev.preventDefault();
    var eq = false;
    var orig_ids_str = localStorage.getItem("orig_ids");
    var orig_ids_arr = orig_ids_str.split(",");
    orig_ids_arr.forEach((val, idx) => {
      if (parseInt(val) !== ids[idx]) {
        // change ord to idx by id===val 
        $.ajax({
        type: "POST",
        url: 'update-ord.php',
        data: { 
          jsondata: JSON.stringify({
            changeto: idx,
            where: ids[idx],
            is: ids[idx]
          })
        },
        success: function (data) {          
            localStorage.setItem("orig_ids", ids);   
            swal("Confirm Saved.", "Sort order saved!", "success");
        }
      });
      }
    });
  });
})();
