var url = window.location;
var orig_index = [];
var ids = [];

$('ul.nav a').filter(function () {
  return this.href == url;
}).parent().addClass('active');

$(function () {
  
  $.ajax({
        type: "POST",
        url: 'get-ids.php',
        success: function (data) {          
          const thedata = JSON.parse(data);
          if (thedata.result === "success") {            
            const f = thedata.ids.map((val) => parseInt(val.id))
            localStorage.setItem("orig_ids", f);            
            ids = f;
          }
          else if(thedata.result === "failed"){
            console.error("[DEBUG] Failed: ", data);
          }          
          
        }
  });
  
  $(".sorted_table").sortable({
  containerSelector: 'table',
  itemPath: '> tbody',
  itemSelector: 'tr',
  placeholder: '<tr class="placeholder"/>',
  onDragStart: function ($item, container, _super) {
    oldIndex = $item.index();
    $item.appendTo($item.parent());
    _super($item, container);
  },
  onDrop: function  ($item, container, _super) {
    var field,
        newIndex = $item.index();

    console.log("newIndex: ", newIndex)
    console.log("oldIndex: ", oldIndex)
    
      var temp = new Array(2);
      temp[0] = ids[newIndex];
      temp[1] = ids[oldIndex];

    if (newIndex < oldIndex) {
        var c = 0;
        var t = 1;
        while ((newIndex + c) <= oldIndex) {
          ids[newIndex + c] = temp[t];
          ++c;
          temp[t] = ids[newIndex + c];          
          t = (t === 1) ? 0 : 1;
        }
        
    }
    if (newIndex > oldIndex) {

        var c = 0;
        var t = 1;
        while ((newIndex - c) >= oldIndex) {
          ids[newIndex - c] = temp[t];
          ++c;
          temp[t] = ids[newIndex - c];          
          t = (t === 1) ? 0 : 1;
        }
    }
    console.log(ids);

    _super($item, container);
  }
});
});

function goDelete(deleteid) {  
  swal("Are you sure you want to delete this profile? ", {
  buttons: {
    cancel: "No",
    catch: {
      text: "Yes!",
      value: "catch",
    }
  },
})
.then((value) => {
  switch (value) { 
    case "catch":
      $.ajax({
        type: "POST",
        url: 'delete-profile.php',
        data: { 
          jsondata: JSON.stringify({
            deleteid
          })
        },
        success: function (data) {          
          const thedata = JSON.parse(data);
          if (thedata.result === "success") {
            swal("Confirm deleted.", "Ok. Profile was deleted!", "success");
          }
          else if(thedata.result === "failed"){
            swal("Delete Failed.", "Profile cannot be deleted. Please try again later. ", "error");
          }
          setTimeout(function(){ window.location.href = 'index.php'; }, 2000);
        }
      });
      break;
 
    default:
      swal("Nothing happened.", "Ok. Profile not deleted!", "info");
      setTimeout(function(){ window.location.href = 'index.php'; }, 2000);
  }
})
.catch(err => {
  if (err) {
    swal("Something went wrong", "The AJAX request failed! Please try again later. ", "error");
  } else {
    swal.stopLoading();
    swal.close();
  }
  setTimeout(function(){ window.location.href = 'index.php'; }, 2000);
});
}

