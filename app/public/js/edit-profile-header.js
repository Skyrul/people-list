$( function() {
    $( "#dobField" ).datepicker({
  dateFormat: "yy-mm-dd"
});
});

$(function () {
      
    $.ajax({
        type: "GET",
        url: 'get-countries.php',
        data: {},
        success: function (data) {       
          const item = JSON.parse(data);
          if (item.result === 'success') {
            var countries = item.countries.map((val, index) => val.country);
            $( "#country" ).autocomplete({
              source: countries,      
              autoFocus: true
            });
          }          
        }
    });
  
    $.ajax({
        type: "GET",
        url: 'get-cities.php',
        data: {},
        success: function (data) {       
          const item = JSON.parse(data);
          if (item.result === 'success') {
            var cities = item.cities.map((val, index) => val.city);
            $( "#city" ).autocomplete({
              source: cities,      
              autoFocus: true
            });
            $( "#state" ).autocomplete({
              source: cities,      
              autoFocus: true
            });
          }          
        }
    });

  
  
});
  