<?php 
include_once '../resource/Database.php';
$ids = null;
$result = "failed";
try{
  $sqlQuery = "SELECT id FROM users ORDER BY id ASC";
  $statement = $db->query($sqlQuery);
  $ids = $statement->fetchAll(PDO::FETCH_ASSOC);  
  $result = "success";
} catch(PDOException $exception) {
  
}

$array = array();
$array['result'] = $result;
$array['ids'] = $ids;

$sendmessage = json_encode($array, JSON_PRETTY_PRINT);

echo $sendmessage;

?>