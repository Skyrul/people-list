<?php 
$page_title = "People Book - Edit Profile";
include_once './partials/headers.php';
include_once './partials/parseProfile.php';
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jodit/3.4.25/jodit.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jodit/3.4.25/jodit.min.js"></script>
<div style="margin-top:2rem;">
<div class="container">
<p><a href="javascript: history.back();">Back</a></p>
<p><a href="forgot_password.php">Reset Password</a></p>
  <section class="col col-lg-7">
  <h2>Edit Profile</h2>
  <div>
    <?php if(isset($result)) { echo $result; } ?>
    <?php if(!empty($form_errors)) { echo show_errors($form_errors); } ?>
  </div>
  <div class="clearfix"></div>

  <?php if(!isset($_SESSION['username'])): ?>
    <p class="lead">
    You are not authorized to view this page <a href="login.php">Log-in</a>
        Not yet a member? <a href="signup.php">Sign up</a>
    </p>
  <?php else: ?>
    <form action="" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label for="emailField">Email</label>
        <input type="text" id="emailField" name="email" class="form-control" value="<?php if(isset($email)) { echo $email; } ?>">
      </div>
      <div class="form-group">
        <label for="usernameField">Username</label>
        <input type="text" id="usernameField" name="username" class="form-control" value="<?php if(isset($username)) { echo $username; } ?>">
      </div>

      <div class="form-group">
        <label for="firstnameField">First name</label>
        <input type="text" id="firstnameField" name="first_name" class="form-control" value="<?php if(isset($first_name)) { echo $first_name; } ?>">
      </div>

      <div class="form-group">
        <label for="lastnameField">Last name</label>
        <input type="text" id="lastnameField" name="last_name" class="form-control" value="<?php if(isset($last_name)) { echo $last_name; } ?>">
      </div>

      <div class="form-group">
        <label for="dobField">Date of birth</label>
        <div class='input-group date'>
               <input type='text' class="form-control" name="date_birth" id="dobField" value="<?php if(isset($date_birth)) { echo date('Y-m-d', strtotime($date_birth)); } ?>" />
               <span class="input-group-addon">
               <span class="glyphicon glyphicon-calendar"></span>
               </span>
        </div>
      </div>

      <div class="form-group">
        <label for="country">Country </label>
        <input id="country" value="<?php if(isset($country)) { echo $country; } ?>" name="country"  class="form-control" />
      </div>

      <div class="form-group">
        <label for="state">State </label>
        <input id="state" value="<?php if(isset($state)) { echo $state; } ?>" name="state"  class="form-control" />
      </div>

      <div class="form-group">
        <label for="city">City </label>
        <input id="city" value="<?php if(isset($city)) { echo $city; } ?>" name="city"  class="form-control" />
      </div>

      <div class="form-group">
      <label for="gender">Gender </label>
      <fieldset class="btn-group" role="group">              
        <label for="male" type="button" class="btn btn-default">
        <input type="radio" name="gender" <?php if(isset($gender) && $gender==='male') { echo 'checked'; } else { echo ''; } ?> value="male" id="male" />
        <label for="male">Male</label>    
        </label>
        <label for="female" type="button" class="btn btn-default">
        <input type="radio" name="gender" <?php if(isset($gender) && $gender==='female') { echo 'checked'; } else { echo ''; } ?> value="female" id="female" />
        <label for="female">Female</label> 
        </label>
        <label for="other" type="button" class="btn btn-default">
        <input type="radio" name="gender" <?php if(isset($gender) && $gender==='other') { echo 'checked'; } else { echo ''; } ?> value="other" id="other" />
        <label for="other">Other</label>
        </label>
      </fieldset>
      </div>

      <div class="form-group">
        <label for="fileField">Replace current picture</label>
        <input type="file" id="fileField" name="avatar">
      </div>

      <div class="form-group">
      <label>Hobbies </label>
      
      <fieldset class="btn-group" role="group">                    
        <label for="cycling" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="cycling" <?php if(in_array("Cycling", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Cycling" />
        <label for="cycling">Cycling</label>    
        </label>

        <label for="swimming" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="swimming" <?php if(in_array("Swimming", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Swimming" />
        <label for="swimming">Swimming</label> 
        </label>

        <label for="running" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="running" <?php if(in_array("Running", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Running" />
        <label for="running">Running</label> 
        </label>

        <label for="walking" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="walking" <?php if(in_array("Walking", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Walking" />
        <label for="walking">Walking</label> 
        </label>

        <label for="bowling" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="bowling" <?php if(in_array("Bowling", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Bowling" />
        <label for="bowling">Bowling</label> 
        </label>

        <label for="skiing" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="skiing" <?php if(in_array("Skiing", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Skiing" />
        <label for="skiing">Skiing</label> 
        </label>

        <label for="shopping" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="shopping" <?php if(in_array("Shopping", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Shopping" />
        <label for="shopping">Shopping</label> 
        </label>

        <label for="sky_diving" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="sky_diving" <?php if(in_array("Sky diving", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Sky diving" />
        <label for="sky_diving">Sky diving</label> 
        </label>

        <label for="reading" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="reading" <?php if(in_array("Reading", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Reading" />
        <label for="reading">Reading</label> 
        </label>

        <label for="television" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="television" <?php if(in_array("Television", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Television" />
        <label for="television">Television</label> 
        </label>

        <label for="drawing" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="drawing" <?php if(in_array("Drawing", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Drawing" />
        <label for="drawing">Drawing</label> 
        </label>

        <label for="horse_riding" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="horse_riding" <?php if(in_array("Horse Riding", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Horse Riding" />
        <label for="horse_riding">Horse Riding</label> 
        </label>

        <label for="fishing" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="fishing" <?php if(in_array("Fishing", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Fishing" />
        <label for="fishing">Fishing</label> 
        </label>

        <label for="surfing" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="surfing" <?php if(in_array("Surfing", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Surfing" />
        <label for="surfing">Surfing</label> 
        </label>

        <label for="climbing" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="climbing" <?php if(in_array("Climbing", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Climbing" />
        <label for="climbing">Climbing</label> 
        </label>


        <label for="other_hobbies" type="button" class="btn btn-default">
        <input type="checkbox" name="hobbies[]" id="other_hobbies" <?php if(in_array("Other", $hobbies)) {echo 'checked';} else {echo '';} ?> value="Other" />
        <label for="other_hobbies">Other</label>
        </label>
      </fieldset>
      </div>


      <div style="width: 100%;">
        <label for="description">Description </label>
        <div id="editor"></div>
      </div>


      <input type="hidden" name="hidden_id" value="<?php if(isset($id)){echo $id;} ?>">
      <input type="hidden" name="token" value="<?php if(function_exists('_token')) {echo _token();} ?>" />
      <button id="updateProfileButton" name="updateProfileButton" class="btn btn-primary pull-right" style="margin-top:2rem;">Update Profile</button>


      </div>
      

    </form>
  <?php endif ?>
  </section>
  
</div>

<script>
  var editor = new Jodit('#editor', {
    theme: "summer"
  });
  editor.value = '<p>start</p>';
  console.log(editor.value)
</script>
<script src="js/edit-profile-header.js"></script>