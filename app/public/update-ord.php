<?php 
include_once '../resource/Database.php';

$data_received = json_decode($_POST['jsondata']);
$changeto = $data_received->changeto;
$where = $data_received->where;
$is = $data_received->is;

$sqlUpdate = "UPDATE id_ord SET ord = :changeto WHERE id = :where";
$statement = $db->prepare($sqlUpdate);
$statement->execute(array(':changeto' => $changeto, ':where' => $where));

$result = "failed";
if($statement->rowCount() == 1) {
  $result = "success";
}

$array = array();
$array['result'] = $result;

$sendmessage = json_encode($array, JSON_PRETTY_PRINT);

echo $sendmessage;

?>