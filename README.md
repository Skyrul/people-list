How to Run

Saving the addresses of people you care about.

1. Install Docker.
2. Open a terminal in the folder you want to store the website in (use File > Open Powershell on windows to open PowerShell in the currently opened folder).
3. Run the command docker run -v ${PWD}:/git alpine/git clone git@gitlab.com:Skyrul/people-list.git . (that final dot is important!).
4. Run docker-compose up —build.
5. Navigate your browser to http://127.0.0.1


